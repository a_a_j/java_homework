package ru.zharikov.homework4;

import java.util.ArrayList;
import java.util.Objects;

public class MathBox {
    private ArrayList <Number> numberList;

    MathBox(Number[] array){
        // в коллекцию добавляем только уникальные значения из переданного массива
        try {
            numberList = new ArrayList<>();
            // сравнение будем производить с помощью вспомогательной коллекции с типом Double
            ArrayList <Double> doubleList = new ArrayList<>();

            for (Number num : array) {
                if (!doubleList.contains(num.doubleValue())) {
                    doubleList.add(num.doubleValue());
                    numberList.add(num);
                }
            }
        }
        catch (NullPointerException e) {
            System.out.println("Ошибка конструктора: массив отсутствует");
        }
    }

    public void splitter(int divider) {
        if (divider > 0) {
            try {
                for (int i = 0; i < numberList.size(); i++) {
                    numberList.set(i, numberList.get(i).doubleValue() / divider);
                }
            } catch (NullPointerException e) {
                System.out.println("Ошибка в методе splitter: ArrayList непроинициализирован");
            }
        }
    }

    public double summator() {
        double vSum = 0;
        try {
            for (Number aNum : numberList) {
                vSum = vSum + aNum.doubleValue();
            }
        }
        catch (NullPointerException e) {
            System.out.println("Ошибка в методе summator: ArrayList непроинициализирован");
        }
        return vSum;
    }

    @Override
    public String toString(){
        String str = "";
        try {
            for (Number aNum : numberList) {
                str = str + aNum + " ";
            }
        }
        catch (NullPointerException e) {
            System.out.println("Ошибка в методе toString: ArrayList непроинициализирован");
        }
        return str;
    }

    public void ascSort() {
        numberList.sort(new AscendingComparator());
    }

    public void descSort() {
        numberList.sort(new DescendingComparator());
    }

    public void replaceElement(Number numOld, Number numNew) {
        if (numberList.contains(numOld)) {
            for (int i = 0; i < numberList.size(); i++) {
                if (numberList.get(i).equals(numOld)) {
                    numberList.set(i, numNew);
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        if (hashCode() != o.hashCode()) return false;
        MathBox otherBox = (MathBox) o;
        ArrayList <Number> otherList = otherBox.numberList;
        if (numberList.size() != otherList.size()) return false;
        // поэлементное сравнение коллекций
        for (int i = 0; i < otherList.size(); i++) {
            if ((numberList.get(i).getClass() != otherList.get(i).getClass()) ||
                (numberList.get(i) != otherList.get(i))) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberList);
    }
}
