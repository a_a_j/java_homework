package ru.zharikov.homework4;

import java.util.Comparator;

public class DescendingComparator implements Comparator<Number> {
    @Override
    public int compare(Number d1, Number d2) {
        Double dd1 = d1.doubleValue();
        Double dd2 = d2.doubleValue();
        return dd2.compareTo(dd1);
    }
}
