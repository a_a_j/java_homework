package ru.zharikov.homework4;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        MathBox mathBox, otherBox;
        Number[] array = new Number[9];
        // заполняем массив разными числами, в том числе повторяя их в разных типах
        array[0] = 43;
        array[1] = 23;
        array[2] = 12;
        array[3] = BigDecimal("0.99");
        array[4] = BigInteger("88");
        array[5] = (Long) 43L;
        array[6] = (byte) 23;
        array[7] = (Double) 51.2;
        array[8] = (short) 85;
        // создаем объект mathBox
        mathBox = new MathBox(array);
        System.out.println("Коллекция с уникальными значениями в mathBox: " + mathBox.toString());
        // создаем объект otherBox с таким же массивом
        otherBox = new MathBox(array);
        System.out.println("Сравнение объектов mathBox и otherBox: " + mathBox.equals(otherBox));
        // заменяем в otherBox элемент по значению
        otherBox.replaceElement(BigDecimal("0.99"), 77);
        System.out.println("Сравнение объектов mathBox и otherBox после замены: " + mathBox.equals(otherBox));

        System.out.println("Сумма элементов коллекции mathBox: " + mathBox.summator());
        mathBox.descSort();
        System.out.println("Упорядоченная по убыванию коллекция в mathBox: " + mathBox.toString());
        mathBox.ascSort();
        System.out.println("Упорядоченная по возрастанию коллекция в mathBox: " + mathBox.toString());
        // поочередное деление элементов
        mathBox.splitter(2);
        System.out.println("Коллекция в mathBox с результатами деления: " + mathBox.toString());
    }

    private static Number BigInteger(String value) {
        BigInteger bi1 = new BigInteger(value);
        return bi1;
    }
    private static Number BigDecimal(String value) {
        BigDecimal bi1 = new BigDecimal(value);
        return bi1;
    }
}
