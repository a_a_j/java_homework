package ru.zharikov.lab_readfile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UniqueWords {
    ArrayList<String> arrayList;

    public UniqueWords(List<String> uniq) {
        this.arrayList = (ArrayList<String>) uniq;
    }

    public HashSet getUniqueWords() {
        HashSet<String> uniqueWords = new HashSet<String>();
        for (String s:arrayList) {
            String[] words = s.split("\\W");

            for (String word : words) {
                uniqueWords.add(word.toLowerCase());
            }
        }
        return uniqueWords;
    }
}
