package ru.zharikov.lab_readfile;

import java.util.Comparator;

public class AscComparator implements Comparator <String> {
    @Override
    public int compare(String s1, String s2) {
        return s1.compareToIgnoreCase(s2);
    }
}

