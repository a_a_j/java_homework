package ru.zharikov.lab_readfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        List<String> list = new ArrayList<>();
        HashSet<String> uniqueWords = new HashSet<String>();
        //Set<String> uniqueWords = new HashSet<>();
        File file =
                new File("/Volumes/Files/Documents/Java/java_homework/src/ru/zharikov/lab_readfile/test.txt");
        Scanner sc = new Scanner(file);

        sc.useDelimiter("\\.");
        while(sc.hasNext()){
            list.add(sc.next());
        }
        sc.close();

        list.sort(new AscComparator());
        System.out.println(list.toString());

        UniqueWords uniq = new UniqueWords(list);
        uniqueWords = uniq.getUniqueWords();

        System.out.println(uniqueWords.toString());
    }
}
