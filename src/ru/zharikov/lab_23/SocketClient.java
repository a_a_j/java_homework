package ru.zharikov.lab_23;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class SocketClient {
    public static void main(String[] args) throws IOException, InterruptedException {
        InetSocketAddress address = new InetSocketAddress("localhost", 8091);
        SocketChannel channel = SocketChannel.open(address);
        channel.configureBlocking(false);
        System.out.println("Sending messages:");
        String[] toSend = new String[]{"First message", "Second message", "Another message", "end"};
        for (String message : toSend) {
            byte[] byteMessage = message.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(byteMessage);
            channel.write(buffer);
            System.out.println(message);
            buffer.clear();
            Thread.sleep(1000);
        }
        channel.close();
    }
}
