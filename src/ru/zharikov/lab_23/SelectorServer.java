package ru.zharikov.lab_23;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Set;

public class SelectorServer {
    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        System.out.println("Selector opened");
        ServerSocketChannel channel = ServerSocketChannel.open();
        InetSocketAddress address = new InetSocketAddress("localhost", 8091);
        channel.bind(address);
        channel.configureBlocking(false);
        int ops = channel.validOps();
        channel.register(selector, ops, null);
        System.out.println("Wait for select");
        selector.select();
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        for (SelectionKey key : selectedKeys) {
            if (key.isAcceptable()) {
                SocketChannel client = channel.accept();
                client.configureBlocking(false);
                client.register(selector, SelectionKey.OP_READ);
                System.out.println("Connection established with client " + client);
            }
        }
        while (selector.isOpen()) {
            System.out.println("Wait for incoming messages");
            selector.select();
            selectedKeys = selector.selectedKeys();
            for (SelectionKey key : selectedKeys) {
                if (key.isReadable()) {
                    SocketChannel client = (SocketChannel) key.channel();
                    client.configureBlocking(false);
                    ByteBuffer buffer = ByteBuffer.allocate(256);
                    client.read(buffer);
                    String out = new String(buffer.array()).trim();
                    System.out.println("Incoming message: " + out);
                    if (out.equals("end")) {
                        client.close();
                        System.out.println("Client closed");
                        selector.close();
                    }
                }
            }
        }
    }
}

