package ru.zharikov.homework_thread_pool;

class Factory {
    private Factorial factorial;

    public Factory(Factorial factorial) {
        this.factorial = factorial;
    }

    void calculate(int value) {
        factorial.calculate(value);
    }
}
