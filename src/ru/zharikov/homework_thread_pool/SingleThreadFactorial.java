package ru.zharikov.homework_thread_pool;

public class SingleThreadFactorial extends Factorial {

    public SingleThreadFactorial() {
        super();
    }

    @Override
    public void calculate(int value) {
        System.out.println("---Однопоточный режим---");
        int resultValue = 1;
        for (int i = 1; i <= value; i++) {
            resultValue *= i;
            System.out.println("Факториал числа " + i + " = " + resultValue);
        }
    }
}
