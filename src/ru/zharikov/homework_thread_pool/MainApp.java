package ru.zharikov.homework_thread_pool;

/* Задача: посчитать факториалы n последовательных чисел
   однопоточно и многопоточно с использованием пула потоков

   UPD: с применением фабричного метода */

public class MainApp {
    public static void main(String[] args) {
        Factory factory = new Factory(new SingleThreadFactorial());
        factory.calculate(7);
        factory = new Factory(new MultiThreadFactorial());
        factory.calculate(7);
    }
}
