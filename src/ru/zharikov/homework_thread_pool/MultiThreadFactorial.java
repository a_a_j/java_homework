package ru.zharikov.homework_thread_pool;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultiThreadFactorial extends Factorial {

    public MultiThreadFactorial() {
        super();
    }

    @Override
    public void calculate(int value) {
        System.out.println("---Многопоточный режим---");
        // для выполнения задачи выделим себе пулл из 5 потоков
        ExecutorService threadPool = Executors.newFixedThreadPool(5);
            /* Предполагается, что для каждого числа от 1 до maxValue
               будет вызван свой поток, вычисляющий его факториал */
        for (int i = 1; i <= value; i++) {
            CountDownLatch latch = new CountDownLatch(1);
            Runnable worker = new CalcThread(latch, i);
            threadPool.execute(worker);
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        threadPool.shutdown();
    }
}
