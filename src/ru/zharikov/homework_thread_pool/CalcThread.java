package ru.zharikov.homework_thread_pool;

import java.util.concurrent.CountDownLatch;

public class CalcThread extends Thread{
    private int maxValue;
    private CountDownLatch latch;

    CalcThread(CountDownLatch latch, int maxValue) {
        this.latch = latch;
        this.maxValue = maxValue;
    }

    @Override
    public void run() {
        int resultValue = 1;
        for (int i = 1; i <= maxValue; i++) {
            resultValue *= i;
        }
        System.out.println("Факториал числа " + maxValue + " = " + resultValue);
        latch.countDown();
    }
}
