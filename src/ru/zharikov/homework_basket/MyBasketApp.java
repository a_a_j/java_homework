package ru.zharikov.homework_basket;

import java.util.List;

public class MyBasketApp {
    public static void main(String[] args) {
        List<String> products;
        MyBasketClass basket = new MyBasketClass();
        // наполняем корзину
        basket.addProduct("Apple", 3);
        basket.addProduct("Banana", 2);
        basket.addProduct("Orange", 1);
        basket.addProduct("Lemon", 7);
        // заменяем кол-во яблок
        basket.updateProductQuantity("Apple", 5);
        // добавляем еще бананов
        basket.addProduct("Banana", 2);
        // смотрим сколько всего бананов сейчас
        System.out.println(basket.getProductQuantity("Banana") + " banana in basket");
        // удаляем лимоны
        basket.removeProduct("Lemon");

        // выводим список всех продуктов в корзине
        products = basket.getProducts();
        for (String product : products) {
            System.out.println(product);
        }

        // очищаем корзину
        basket.clear();
    }
}
