package ru.zharikov.homework_basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBasketClass implements Basket {
    Map basket;

    public MyBasketClass() {
        basket = new HashMap<String, Integer>();
    }

    @Override
    public void addProduct(String product, int quantity) {
        int qty;
        if (basket.containsKey(product)) {
            qty = quantity + (int)basket.get(product);
        } else {
            qty = quantity;
        }
        basket.put(product, qty);
        System.out.println("Added " + quantity + " " + product + " to basket");
    }

    @Override
    public void removeProduct(String product) {
        if (basket.containsKey(product)) {
            basket.remove(product);
            System.out.println(product + " was removed from basket");
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (basket.containsKey(product)) {
            basket.replace(product, quantity);
        }
    }

    @Override
    public void clear() {
        basket.clear();
        System.out.println("Basket is empty");
    }

    @Override
    public List<String> getProducts() {
        List<String> products = new ArrayList<>();
        if (!basket.isEmpty()) {
            products.addAll(basket.keySet());
        }
        return products;
    }

    @Override
    public int getProductQuantity(String product) {
        int qty = 0;
        if (basket.containsKey(product)) {
            qty = (int) basket.get(product);
        };
        return qty;
    }
}
