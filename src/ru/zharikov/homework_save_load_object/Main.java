package ru.zharikov.homework_save_load_object;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/* Задание 2 (пакет 2)
Написать программу, которая выполняет сохранение в файл и чтение из файла простого
(содержащего только поля-примитивы и String) объекта.
Не использовать для этого ObjectOutput и ObjectInput
(реализовать механизм сериализации-десериализации самостоятельно).
Скорее всего, вам пригодится рефлексия (reflections API).*/

public class Main {
    public static void main(String[] args) throws IOException, IllegalAccessException, InvocationTargetException {
        // создаем объект класса Person со значениями
        Person ivan = new Person("Ivan", 25, 1.73);
        // создаем объект класса Person без значений
        Person unknown = new Person();

        String filePath = "src/ru/zharikov/homework_save_load_object/object.txt";
        // сохраняем первый объект в файл
        ObjectBox objBox = new ObjectBox(ivan);
        objBox.SaveToFile(filePath);

        // загружаем значения во второй объект из файла
        objBox.LoadFromFile(unknown, filePath);

        // выводим для проверки в консоль
        System.out.println(unknown.getName());
        System.out.println(unknown.getAge());
        System.out.println(unknown.getHeight());
    }
}
