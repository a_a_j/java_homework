package ru.zharikov.homework_save_load_object;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class ObjectBox {
   Object obj;

   // конструктор
   public ObjectBox(Object obj) {
        this.obj = obj;
    }

   // сохранение объекта в файл
   public void SaveToFile(String filePath) throws IOException, IllegalAccessException {
       Class clazz = obj.getClass();
       FileWriter writer = new FileWriter(filePath);
       for (Field f : clazz.getDeclaredFields()) {
           writer.write(f.getName() + "=" + f.get(obj).toString()+"\r\n");
            //System.out.println(f.getName() + ": " + f.get(obj).toString());
       }
       writer.close();
   }

   // загрузка полей из файла в объект
   public void LoadFromFile(Object someObject, String filePath) throws FileNotFoundException, InvocationTargetException, IllegalAccessException {
       Class clazz = obj.getClass();
       File file = new java.io.File(filePath);
       Scanner scanner = new Scanner(file);
       String current;
       String fieldName;
       String fieldValue;
       int divider;
       // сканируем файл построчно
       while(scanner.hasNextLine()){
            current = scanner.nextLine();
            divider = current.indexOf("=");
            // вычленяем из каждой строки имя поля и его значение
            fieldName = current.substring(0, divider);
            fieldValue = current.substring(divider+1, current.length());
            // проверяем объявленные поля в переданном "пустом" объекте
            for (Field f : clazz.getDeclaredFields()) {
                // если поле совпадает по имени
                if (f.getName().equals(fieldName)) {
                    // проверяем, есть ли у объекта сеттер для этого поля
                    for (Method m : clazz.getDeclaredMethods()) {
                        if (m.getName().toLowerCase().equals("set"+fieldName.toLowerCase())) {
                            // если сеттер есть, то вызываем его, передавая значение из файла
                            Object paramsObj[] = {toObject(f.getType(), fieldValue)};
                            try {
                                m.invoke(someObject, paramsObj);
                            }
                            catch (IllegalArgumentException e) {}
                        }
                    }
                }
            }
       }
   }

   // метод класс-конвертер
   public static Object toObject(Class clazz, String value)  {
       if (clazz == Boolean.class) return Boolean.parseBoolean(value);
       if (clazz == Byte.class) return Byte.parseByte(value);
       if (clazz == Short.class) return Short.parseShort(value);
       if (clazz == Integer.class) return Integer.parseInt(value);
       if (clazz.toString().equals("int")) return (int) Integer.parseInt(value);
       if (clazz == Long.class) return Long.parseLong(value);
       if (clazz == Float.class) return Float.parseFloat(value);
       if (clazz == Double.class) return Double.parseDouble(value);
       if (clazz.toString().equals("double")) return (double) Double.parseDouble(value);
       return value;
   }
}
