package ru.zharikov.homework3.HelloWorldException;

import ru.zharikov.homework3.MyExceptionClass.MyExceptionClass;

import java.util.Scanner;

public class HelloWorldException {
    public static void main(String[] args) throws MyExceptionClass {
        int[] someArray = new int[4];
        System.out.println("[1] - Смоделировать ошибку «NullPointerException»");
        System.out.println("[2] - Смоделировать ошибку «ArrayIndexOutOfBoundsException»");
        System.out.println("[3] - Вызвать свой вариант ошибки через оператор throw");
        int console = new Scanner(System.in).nextInt();
        switch(console) {
            case 1:
                testMethod(null);
                break;
            case 2:
                someArray[0] = 123;
                testMethod(someArray);
                break;
            case 3:
                someArray[0] = 123;
                someArray[1] = 666;
                testMethod(someArray);
                break;
            default:
                break;
        }
    }

    private static void testMethod(int[] pArray) throws MyExceptionClass {
        for (int i=0; i < 5; i++) {
            System.out.println(pArray[i]);
            if (pArray[i] == 666) {
                throw new MyExceptionClass();
            }
        }
    }
}
