package ru.zharikov.lab_client_server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public static final int PORT = 8080;

    public static void main(String[] args) throws IOException {
        try (ServerSocket sSocket = new ServerSocket(PORT)) {
            System.out.println("Server is running...");
            try (Socket socket = sSocket.accept();
                 BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true)) {
                String inputStr = "";
                String outputStr = "";
                while (true) {
                    inputStr = in.readLine();
                    if (!inputStr.isEmpty()) {
                        System.out.println(inputStr);
                        outputStr = new Scanner(System.in).next();
                        out.println(outputStr);
                    }
                }
            }
        }
    }
}
