package ru.zharikov.lab_client_server;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        InetAddress addr = InetAddress.getByName("127.0.0.1");
        System.out.println(addr);
        Socket socket = new Socket(addr, Server.PORT);
        System.out.println("Client is running...");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            String inputStr = "";
            String outputStr = "";

            outputStr = new Scanner(System.in).next();
            out.println(outputStr);

            while (true) {
                while (inputStr.isEmpty()) {
                    inputStr = in.readLine();
                    System.out.println(inputStr);
                }
                inputStr = "";
                outputStr = new Scanner(System.in).next();
                out.println(outputStr);
            }

        } finally {
            socket.close();
        }
    }
}
