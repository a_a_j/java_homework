package ru.zharikov.homework2.vending;


import ru.zharikov.homework2.vending.drinks.Drink;
import ru.zharikov.homework2.vending.drinks.ColdDrink;
import ru.zharikov.homework2.vending.drinks.HotDrink;

import java.util.Scanner;

public class VendingMachine {
    private double money = 0;
    private Drink[] currentDrinks;
    private Drink[] hotDrinks;
    private Drink[] coldDrinks;
    private int section;
    public enum MenuItems {
        AddMoney, HotDrinks, ColdDrinks, Exit
    }
    MenuItems selectedItem;

    VendingMachine() {
        fillMachine();
        mainMenu();
    }

    private void fillMachine() {
        HotDrink tea = new HotDrink("Чай", 25);
        HotDrink coffee = new HotDrink("Кофе", 35);
        hotDrinks = new HotDrink[]{tea, coffee};
        ColdDrink cola = new ColdDrink("Кола", 50);
        ColdDrink fanta = new ColdDrink("Фанта", 50);
        coldDrinks = new ColdDrink[]{cola, fanta};
    }

    private void mainMenu(){
        System.out.println("Ваш баланс: " + money + " р.");
        System.out.println("[0] Добавить деньги");
        System.out.println("[1] Горячие напитки");
        System.out.println("[2] Прохладительные напитки");
        System.out.println("[3] Выход");
        selectedItem = MenuItems.values()[new Scanner(System.in).nextInt()];
        switch(selectedItem) {
            case AddMoney:
                Section0();
                break;
            case HotDrinks:
                Section1();
                break;
            case ColdDrinks:
                Section2();
                break;
            case Exit:
                System.exit(0);
            default:
                break;
        }
    }

    private void Section0() {
        System.out.println("Введите сумму к пополнению:");
        money = new Scanner(System.in).nextInt();
        addMoney(money);
        mainMenu();
    }
    private void Section1() {
        System.out.println("Горячие напитки на выбор:");
        setCurrentDrinks(hotDrinks);
    }
    private void Section2() {
        System.out.println("Прохладительные напитки на выбор:");
        setCurrentDrinks(coldDrinks);
    }

    private void addMoney(double money) {
        this.money = money;
    }

    private Drink getDrink(int key) {
        if (key < currentDrinks.length){
            return currentDrinks[key];
        } else {
            return null;
        }
    }

    private void giveMeADrink(int key) {
        if (key > hotDrinks.length-1 || key < 0) {
            System.exit(0);
        }
        if (this.money > 0) {
            Drink drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    money -= drink.getPrice();
                } else {
                    System.out.println("Недостаточно средств!");
                }
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    private void setCurrentDrinks(Drink[] currentDrinks) {
        this.currentDrinks = currentDrinks;
        for (int i = 0; i < currentDrinks.length; i++) {
            System.out.println("["+i+"] " + currentDrinks[i].getTitle() + " - " + currentDrinks[i].getPrice() + " р.");
        }
        section = new Scanner(System.in).nextInt();
        giveMeADrink(section);
        mainMenu();
    }

}
