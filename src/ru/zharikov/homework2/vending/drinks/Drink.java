package ru.zharikov.homework2.vending.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}