package ru.zharikov.homework_OOP.MusicStoreApp;

// Библиотека музыкальных исполнителей и альбомов

import java.util.ArrayList;
import java.util.Scanner;

public class MusicStoreApp {
    public static ArrayList <Artist> artistList = new ArrayList();
    public static ArrayList <Album> albumList = new ArrayList();
    private static Artist artist;
    private static Album album;

    public static void main(String[] args) {
        mainMenu();
    }

    private static void mainMenu() {
        String artistName;
        String albumName;
        String genre;
        String result;
        int year;
        double price;

        System.out.println("[1] - Добавить исполнителя");
        System.out.println("[2] - Добавить альбом");
        System.out.println("[3] - Вывести список всех альбомов");
        System.out.println("[4] - Вывести список всех артистов");
        System.out.println("[5] - Найти альбом по названию");
        System.out.println("[6] - Найти исполнителя по названию");
        int s = new Scanner(System.in).nextInt();
        switch (s) {
            case 1:
                System.out.println("Введите название артиста:");
                artistName = new Scanner(System.in).next();
                System.out.println("Введите жанр:");
                genre = new Scanner(System.in).next();
                artist = new Artist(artistName, genre);
                // добавляем в базу исполнителя
                artistList.add(artist);
                System.out.println(artist.getArtistName() + " добавлен.");
                System.out.println();
                break;
            case 2:
                System.out.println("Введите название артиста:");
                artistName = new Scanner(System.in).next();
                System.out.println("Введите название альбома:");
                albumName = new Scanner(System.in).next();
                System.out.println("Введите год релиза:");
                year = new Scanner(System.in).nextInt();
                System.out.println("Введите жанр:");
                genre = new Scanner(System.in).next();
                album = new Album(artistName, albumName, year, genre);
                // добавляем в базу и альбом и исполнителя
                albumList.add(album);
                artistList.add((Artist) album);
                System.out.println(album.getAlbumName() + " добавлен.");
                System.out.println();
                break;
            case 3:
                if (albumList.size() == 0) {
                    System.out.println("Нет альбомов");
                    System.out.println();
                } else {
                    for (int i = 0; i < albumList.size(); i++) {
                        System.out.println("Название: " + albumList.get(i).getAlbumName());
                        System.out.println("Исполнитель: " + albumList.get(i).getArtistName());
                        System.out.println("Год: " + albumList.get(i).getYear());
                        System.out.println("Жанр: " + albumList.get(i).getGenre());
                        System.out.println();
                    }
                }
                break;
            case 4:
                if (artistList.size() == 0) {
                    System.out.println("Нет исполнителей");
                    System.out.println();
                } else {
                    for (int i = 0; i < artistList.size(); i++) {
                        System.out.println("Имя: " + artistList.get(i).getArtistName());
                        System.out.println("Жанр: " + artistList.get(i).getGenre());
                        System.out.println();
                    }
                }
                break;
            case 5:
                System.out.println("Введите название альбома:");
                albumName = new Scanner(System.in).next();
                result = "Альбом " + albumName + " не найден в магазине";
                for (int i = 0; i < albumList.size(); i++) {
                    if (albumList.get(i).getAlbumName().toLowerCase().equals(albumName.toLowerCase())) {
                        result = "Альбом " + albumName + " найден в магазине";
                        break;
                    }
                }
                System.out.println(result);
                break;
            case 6:
                System.out.println("Введите название исполнителя:");
                artistName = new Scanner(System.in).next();
                result = "Исполнитель " + artistName + " не найден в магазине";
                for (int i = 0; i < artistList.size(); i++) {
                    if (artistList.get(i).getArtistName().toLowerCase().equals(artistName.toLowerCase())) {
                        result = "Исполнитель " + artistName + " найден в магазине";
                        break;
                    }
                }
                System.out.println(result);
                break;
            default:
                System.exit(0);
        }
        mainMenu();
    }
}
