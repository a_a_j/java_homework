package ru.zharikov.homework_OOP.MusicStoreApp;

public class Artist {
    private String artistName;
    private String genre;

    public Artist(String artistName, String genre) {
        this.artistName = artistName;
        this.genre = genre;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getGenre() {
        return genre;
    }

    public void setArtistName(String name) {
        this.artistName = name;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
