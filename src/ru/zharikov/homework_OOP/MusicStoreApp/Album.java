package ru.zharikov.homework_OOP.MusicStoreApp;

// класс Альбом наследует Исполнителя

public class Album extends Artist {
    private String albumName;
    private int year;

    public Album(String artistName, String albumName, int year, String genre) {
        super(artistName, genre);
        this.albumName = albumName;
        this.year = year;
    }

    public String getAlbumName() {
        return albumName;
    }

    public int getYear() {
        return year;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
