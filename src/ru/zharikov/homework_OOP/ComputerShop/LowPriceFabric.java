package ru.zharikov.homework_OOP.ComputerShop;

public class LowPriceFabric implements PriceFabric  {
    public LowPriceFabric() {
    }

    @Override
    public Laptop createLaptop(Company company) {
        return new Laptop(company, "13 inch", "4GB", "HDD 512GB", "i3 1600mhz", 399.99);
    }

    @Override
    public DesktopPC createDesktopPC(Company company) {
        return new DesktopPC(company, "17 inch", "8GB", "HDD 512GB", "i3 1900mhz", 499.99);
    }

    @Override
    public Tablet createTablet(Company company) {
        return new Tablet(company, "8 inch", "4GB", "SSD 128GB", "Snapdragon 625", 299.99);
    }
}
