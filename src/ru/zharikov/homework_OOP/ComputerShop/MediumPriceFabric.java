package ru.zharikov.homework_OOP.ComputerShop;

public class MediumPriceFabric implements PriceFabric {
    public MediumPriceFabric() {
    }

    @Override
    public Laptop createLaptop(Company company) {
        return new Laptop(company, "15 inch", "8GB", "SSD 128GB", "i5 2800mhz", 699.99);
    }

    @Override
    public DesktopPC createDesktopPC(Company company) {
        return new DesktopPC(company, "19 inch", "8GB", "HDD 1TB", "i5 3100mhz", 599.99);
    }

    @Override
    public Tablet createTablet(Company company) {
        return new Tablet(company, "10 inch", "8GB", "SSD 256GB", "Snapdragon 820", 399.99);
    }
}
