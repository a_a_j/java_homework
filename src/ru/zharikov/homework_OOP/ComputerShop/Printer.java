package ru.zharikov.homework_OOP.ComputerShop;

public class Printer {

    public Printer() {
    }

    public void printSpecs(PriceFabric priceFabric, Company company) {
        System.out.println("--- " + priceFabric.getClass().getSimpleName() + " ---");
        Device device = priceFabric.createLaptop(company);
        System.out.println(device.about());

        device = priceFabric.createDesktopPC(company);
        System.out.println(device.about());

        device = priceFabric.createTablet(company);
        System.out.println(device.about());
    }
}
