package ru.zharikov.homework_OOP.ComputerShop;

public interface PriceFabric {
    Laptop createLaptop(Company company);
    DesktopPC createDesktopPC(Company company);
    Tablet createTablet(Company company);
}
