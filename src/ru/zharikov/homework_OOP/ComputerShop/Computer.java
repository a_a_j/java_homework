package ru.zharikov.homework_OOP.ComputerShop;

public abstract class Computer implements Device {
    private Company company;
    private String monitor;
    private String memory;
    private String hardDrive;
    private String CPU;
    private Double Price;

    public Computer(Company company, String monitor, String memory, String hardDrive, String CPU, Double Price) {
        this.company = company;
        this.monitor = monitor;
        this.memory = memory;
        this.hardDrive = hardDrive;
        this.CPU = CPU;
        this.Price = Price;
    }

    @Override
    public String about() {
        return  "company = " + company +
                ", monitor = " + monitor +
                ", memory = " + memory +
                ", hardDrive = " + hardDrive  +
                ", CPU = " + CPU +
                ", Price = " + Price + "$";
    }
}
