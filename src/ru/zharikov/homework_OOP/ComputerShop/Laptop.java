package ru.zharikov.homework_OOP.ComputerShop;

public class Laptop extends Computer {

    public Laptop(Company company, String monitor, String memory, String hardDrive, String CPU, Double Price) {
        super(company, monitor, memory, hardDrive, CPU, Price);
    }

    @Override
    public String about() {
        return "Laptop: " + super.about();
    }
}
