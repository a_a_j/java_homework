package ru.zharikov.homework_OOP.ComputerShop;

public class DesktopPC extends Computer {

    public DesktopPC(Company company, String monitor, String memory, String hardDrive, String CPU, Double Price) {
        super(company, monitor, memory, hardDrive, CPU, Price);
    }

    @Override
    public String about() {
        return "Desktop PC: " + super.about();
    }
}
