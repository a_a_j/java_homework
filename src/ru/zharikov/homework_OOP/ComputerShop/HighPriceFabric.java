package ru.zharikov.homework_OOP.ComputerShop;

public class HighPriceFabric implements PriceFabric {
    public HighPriceFabric() {
    }

    @Override
    public Laptop createLaptop(Company company) {
        return new Laptop(company, "17 inch", "16GB", "SSD 512GB", "i7 3200mhz", 1199.99);
    }

    @Override
    public DesktopPC createDesktopPC(Company company) {
        return new DesktopPC(company, "24 inch", "16GB", "SSD 1TB", "i7 3400mhz", 999.99);
    }

    @Override
    public Tablet createTablet(Company company) {
        return new Tablet(company, "12 inch", "16GB", "SSD 512GB", "Snapdragon 845", 599.99);
    }
}
