package ru.zharikov.homework_OOP.ComputerShop;

public class Tablet extends Computer {

    public Tablet(Company company, String monitor, String memory, String hardDrive, String CPU, Double Price) {
        super(company, monitor, memory, hardDrive, CPU, Price);
    }

    @Override
    public String about() {
        return "Tablet: " + super.about();
    }
}
