package ru.zharikov.homework_OOP.ComputerShop;

public interface Device {
    String about();
}
