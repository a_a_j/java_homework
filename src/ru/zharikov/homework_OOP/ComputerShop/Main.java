package ru.zharikov.homework_OOP.ComputerShop;

public class Main {
    public static void main(String[] args) {
        Printer printer = new Printer();

        PriceFabric priceFabric = new LowPriceFabric();
        printer.printSpecs(priceFabric, Company.XIOMI);

        priceFabric = new MediumPriceFabric();
        printer.printSpecs(priceFabric, Company.MICROSOFT);

        priceFabric = new HighPriceFabric();
        printer.printSpecs(priceFabric, Company.APPLE);

    }
}
