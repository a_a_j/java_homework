package ru.zharikov.lab_10_threads;

import java.util.ArrayList;
import java.util.List;

public class MainApp {
    public static void main(String[] args) {
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Thread myThread = new MyThread(i);
            threads.add(myThread);
            myThread.start();
        }

        CheckTread checkTread = new CheckTread(threads);
        checkTread.start();

    }
}
