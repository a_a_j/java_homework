package ru.zharikov.lab_10_threads;

import java.util.List;

public class CheckTread extends Thread {
    List<Thread> threads;
    public CheckTread (List<Thread> threads) {
        this.threads = threads;
    }

    @Override
    public void run() {
        for (Thread t: threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Все потоки завершились");
    }
}
