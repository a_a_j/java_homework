package ru.zharikov.lab_10_threads;

public class MyThread extends Thread {
    public int num;

    public MyThread (int num) {
        this.num = num;
    }

    @Override
    public void run() {
        System.out.println("Поток "+num+" стартовал");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
