package ru.zharikov.lab_27.AbstractFabric;

public interface Car {
    void Drive();
    String getName();
}
