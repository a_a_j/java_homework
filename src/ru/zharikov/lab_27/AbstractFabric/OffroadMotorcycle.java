package ru.zharikov.lab_27.AbstractFabric;

public class OffroadMotorcycle implements Motorcycle {
    private String name;
    private Integer clearance;
    private Integer suspension;
    private final static Integer wheels = 2;

    public OffroadMotorcycle (String name, Integer clearance, Integer suspension) {
        this.name = name;
        this.clearance = clearance;
        this.suspension = suspension;
    }

    @Override
    public void Drive() {
        System.out.println(getName() + " off road drive!");
    }

    public String getName() {
        return name;
    }
}
