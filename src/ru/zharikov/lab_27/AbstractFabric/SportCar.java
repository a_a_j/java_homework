package ru.zharikov.lab_27.AbstractFabric;

public class SportCar implements Car {
    private String name;
    private Integer clearance;
    private Integer suspension;
    private final static Integer wheels = 4;

    public SportCar (String name, Integer clearance, Integer suspension) {
        this.name = name;
        this.clearance = clearance;
        this.suspension = suspension;
    }

    @Override
    public void Drive() {
        System.out.println(getName() + " fast drive!");
    }

    @Override
    public String getName() {
        return name;
    }
}
