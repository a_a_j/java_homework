package ru.zharikov.lab_27.AbstractFabric;

public interface Fabric {
    void Fabric();
    Car createCar(String name, Integer clearance, Integer suspension);
    Motorcycle createMotorcycle(String name, Integer clearance, Integer suspension);
    Velocycle createVelocycle(String name, Integer clearance, Integer suspension);
}
