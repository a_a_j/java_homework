package ru.zharikov.lab_27.AbstractFabric;

public class OffroadVelocycle implements Velocycle {
    private String name;
    private Integer clearance;
    private Integer suspension;
    private final static Integer wheels = 2;

    public OffroadVelocycle (String name, Integer clearance, Integer suspension) {
        this.name = name;
        this.clearance = clearance;
        this.suspension = suspension;
    }

    @Override
    public void Drive() {
        System.out.println(getName() + " off road drive!");
    }

    public String getName() {
        return name;
    }
}
