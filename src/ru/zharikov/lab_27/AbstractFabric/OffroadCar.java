package ru.zharikov.lab_27.AbstractFabric;

public class OffroadCar implements Car {
    private String name;
    private Integer clearance;
    private Integer suspension;
    private final static Integer wheels = 4;

    public OffroadCar(String name, Integer clearance, Integer suspension) {
        this.name = name;
        this.clearance = clearance;
        this.suspension = suspension;
    }

    @Override
    public void Drive() {
        System.out.println(getName() + " off road drive!");
    }

    @Override
    public String getName() {
        return name;
    }
}
