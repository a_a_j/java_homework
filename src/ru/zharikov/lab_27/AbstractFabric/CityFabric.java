package ru.zharikov.lab_27.AbstractFabric;

public class CityFabric implements Fabric {
    @Override
    public void Fabric() {

    }

    @Override
    public Car createCar(String name, Integer clearance, Integer suspension) {
        return new CityCar(name, clearance, suspension);
    }

    @Override
    public Motorcycle createMotorcycle(String name, Integer clearance, Integer suspension) {
        return new CityMotorcycle(name, clearance, suspension);
    }

    @Override
    public Velocycle createVelocycle(String name, Integer clearance, Integer suspension) {
        return new CityVelocycle(name, clearance, suspension);
    }
}
