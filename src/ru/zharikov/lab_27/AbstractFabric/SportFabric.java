package ru.zharikov.lab_27.AbstractFabric;

public class SportFabric implements Fabric {
    @Override
    public void Fabric() {

    }

    @Override
    public Car createCar(String name, Integer clearance, Integer suspension) {
        return new SportCar(name, clearance, suspension);
    }

    @Override
    public Motorcycle createMotorcycle(String name, Integer clearance, Integer suspension) {
        return new SportMotorcycle(name, clearance, suspension);
    }

    @Override
    public Velocycle createVelocycle(String name, Integer clearance, Integer suspension) {
        return new SportVelocycle(name, clearance, suspension);
    }
}
