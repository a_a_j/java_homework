package ru.zharikov.lab_27.AbstractFabric;

public class OffroadFabric implements Fabric {

    @Override
    public void Fabric() {

    }

    @Override
    public Car createCar(String name, Integer clearance, Integer suspension) {
        return new OffroadCar(name, clearance, suspension);
    }

    @Override
    public Motorcycle createMotorcycle(String name, Integer clearance, Integer suspension) {
        return new OffroadMotorcycle(name, clearance, suspension);
    }

    @Override
    public Velocycle createVelocycle(String name, Integer clearance, Integer suspension) {
        return new OffroadVelocycle(name, clearance, suspension);
    }
}
