package ru.zharikov.lab_27.AbstractFabric;

public class Main {
    public static void main(String[] args) {
        Fabric Fabric = new SportFabric();
        Car ferrariCar = Fabric.createCar("Ferrari", 1, 9);
        ferrariCar.Drive();

        Fabric = new CityFabric();
        Velocycle schwinn = Fabric.createVelocycle("Schwinn", 7, 2);
        schwinn.Drive();
    }

}
