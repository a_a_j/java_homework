package ru.zharikov.lab_27.Immutable;

public class ImmutableClass {
    private final Integer id;
    private final String  name;
    private final MyComplexType myComplexType;

    public ImmutableClass(Integer id, String name, MyComplexType myComplexType) {
        this.id = id;
        this.name = name;
        this.myComplexType = new MyComplexType(myComplexType.getInteger(), myComplexType.toString());
    }

    public Integer getId() {
        Integer newId = id;
        return newId;
    }

    public String getName() {
        String newName = name;
        return newName;
    }

    public MyComplexType getMyComplexType() {
        MyComplexType newComplexType = new MyComplexType(myComplexType.getInteger(), myComplexType.getString());
        return newComplexType;
    }
}
