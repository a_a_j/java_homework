package ru.zharikov.lab_27.Immutable;

public class MyComplexType {
    private Integer integer;
    private String string;

    public MyComplexType(Integer integer, String string) {
        this.integer = integer;
        this.string = string;
    }

    public Integer getInteger() {
        return integer;
    }

    public String getString() {
        return string;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public void setString(String string) {
        this.string = string;
    }
}
