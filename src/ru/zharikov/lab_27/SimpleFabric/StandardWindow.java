package ru.zharikov.lab_27.SimpleFabric;

public class StandardWindow extends Window {
    public StandardWindow(String caption, Integer width, Integer height) {
        super(caption, width, height);
        System.out.println("Render standard window");
    }
}
