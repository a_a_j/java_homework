package ru.zharikov.lab_27.SimpleFabric;

public abstract class Window {
    private String caption;
    private Integer width;
    private Integer height;

    public Window(String caption, Integer width, Integer height) {
        this.caption = caption;
        this.width = width;
        this.height = height;
    }

    //void Render(String caption, Integer width, Integer height);
}
