package ru.zharikov.lab_27.SimpleFabric;

public class Fabric {
    private Window window;

    public Fabric() {
    }

    public void createWindow(String windowType) {
        switch(windowType) {
            case "standart":
                window = new StandardWindow("Standart window", 500, 200);
                break;
            case "dialog":
                window = new DialogWindow("Dialog window", 500, 200);
                break;
            case "modal":
                window = new ModalWindow("Dialog window", 500, 200);
                break;
            default:
                break;
        }
    }
}
