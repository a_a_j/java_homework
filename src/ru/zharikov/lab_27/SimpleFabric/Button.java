package ru.zharikov.lab_27.SimpleFabric;

public class Button {
    Integer x;
    Integer y;
    String caption;

    public Button(Integer x, Integer y, String caption) {
        this.x = x;
        this.y = y;
        this.caption = caption;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
}
