package ru.zharikov.lab_27.Builder.Model;

public class Wheels {
    String name;

    public Wheels(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Wheels{" +
                "name='" + name + '\'' +
                '}';
    }
}
