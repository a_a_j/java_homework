package ru.zharikov.lab_27.Builder.Model;

public class Salon {
    String name;

    public Salon(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Salon{" +
                "name='" + name + '\'' +
                '}';
    }
}
