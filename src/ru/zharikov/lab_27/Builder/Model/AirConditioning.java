package ru.zharikov.lab_27.Builder.Model;

public class AirConditioning {
    String name;

    public AirConditioning(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AirConditioning{" +
                "name='" + name + '\'' +
                '}';
    }
}
