package ru.zharikov.lab_27.Builder;

import ru.zharikov.lab_27.Builder.Model.*;

public class CarBuilder implements BuilderClass {
    private Wheels wheels;
    private Salon salon;
    private Headlights headlights;
    private Engine engine;
    private AudioSystem audioSystem;
    private AirConditioning airConditioning;

    public CarBuilder() {
    }

    @Override
    public void setHeadlights(Headlights headlights) {
        this.headlights = headlights;
    }

    @Override
    public void setSalon(Salon salon) {
        this.salon = salon;
    }

    @Override
    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public void setAirConitioning(AirConditioning airConitioning) {
        this.airConditioning = airConitioning;
    }

    @Override
    public void setAudioSystem(AudioSystem audioSystem) {
        this.audioSystem = audioSystem;
    }

    public SportCar getResult() {
        return new SportCar(wheels, salon, headlights, engine, audioSystem, airConditioning);
    }
}
