package ru.zharikov.lab_27.Builder;

import ru.zharikov.lab_27.Builder.Model.*;

public interface BuilderClass {
    void setHeadlights(Headlights headlights);
    void setSalon(Salon salon);
    void setWheels(Wheels wheels);
    void setEngine(Engine engine);
    void setAirConitioning(AirConditioning airConitioning);
    void setAudioSystem(AudioSystem audioSystem);
}
