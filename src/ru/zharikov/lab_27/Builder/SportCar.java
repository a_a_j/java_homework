package ru.zharikov.lab_27.Builder;

import ru.zharikov.lab_27.Builder.Model.*;

public class SportCar implements Car {
    Wheels wheels;
    Salon salon;
    Headlights headlights;
    Engine engine;
    AudioSystem audioSystem;
    AirConditioning airConditioning;

    public SportCar(Wheels wheels, Salon salon, Headlights headlights, Engine engine, AudioSystem audioSystem, AirConditioning airConditioning) {
        this.wheels = wheels;
        this.salon = salon;
        this.headlights = headlights;
        this.engine = engine;
        this.audioSystem = audioSystem;
        this.airConditioning = airConditioning;
    }

    public Wheels getWheels() {
        return wheels;
    }

    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }

    public Salon getSalon() {
        return salon;
    }

    public void setSalon(Salon salon) {
        this.salon = salon;
    }

    public Headlights getHeadlights() {
        return headlights;
    }

    public void setHeadlights(Headlights headlights) {
        this.headlights = headlights;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public AudioSystem getAudioSystem() {
        return audioSystem;
    }

    public void setAudioSystem(AudioSystem audioSystem) {
        this.audioSystem = audioSystem;
    }

    public AirConditioning getAirConditioning() {
        return airConditioning;
    }

    public void setAirConditioning(AirConditioning airConditioning) {
        this.airConditioning = airConditioning;
    }

    @Override
    public Car clone() {
        return new SportCar(this.wheels, this.salon, this.headlights, this.engine,
                            this.audioSystem, this.airConditioning);
    }

    @Override
    public String toString() {
        return "SportCar{" +
                "wheels=" + wheels.toString() +
                ", salon=" + salon +
                ", headlights=" + headlights +
                ", engine=" + engine +
                ", audioSystem=" + audioSystem +
                ", airConditioning=" + airConditioning +
                '}';
    }
}
