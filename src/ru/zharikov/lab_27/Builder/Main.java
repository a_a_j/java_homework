package ru.zharikov.lab_27.Builder;

import ru.zharikov.lab_27.Builder.Model.AirConditioning;
import ru.zharikov.lab_27.Builder.Model.Salon;
import ru.zharikov.lab_27.Builder.Model.Wheels;

public class Main {
    public static void main(String[] args) {
        BuilderClass builderClass = new CarBuilder();
        builderClass.setWheels(new Wheels("16"));
        builderClass.setSalon(new Salon("Leather"));
        builderClass.setAirConitioning(new AirConditioning("Climate control"));
        SportCar sportCar = ((CarBuilder) builderClass).getResult();

        SportCar clonedSportCar = (SportCar) sportCar.clone();

        System.out.println(sportCar.toString());
        System.out.println(clonedSportCar.toString());
    }
}
