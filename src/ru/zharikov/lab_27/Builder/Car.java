package ru.zharikov.lab_27.Builder;

public interface Car {
    public abstract Car clone();
}
