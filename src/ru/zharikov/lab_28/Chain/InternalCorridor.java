package ru.zharikov.lab_28.Chain;

public class InternalCorridor extends Chain {

    public InternalCorridor() {
    }

    @Override
    public boolean check(Tourist tourist) {
        CheckPassport checkPassport = new CheckPassport();
        checkPassport.linkWith(new CheckTicket());
        return checkPassport.check(tourist);
    }
}
