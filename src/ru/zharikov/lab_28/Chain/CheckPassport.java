package ru.zharikov.lab_28.Chain;

public class CheckPassport extends Chain {

    public boolean check(Tourist tourist) {
        if (tourist.getHasPassport()) {
            return checkNext(tourist);
        } else {
            return false;
        }
    }
}
