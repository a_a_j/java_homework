package ru.zharikov.lab_28.Chain;

public class CheckVisa extends Chain {

    public boolean check(Tourist tourist) {
        if (tourist.getHasVisa()) {
            return checkNext(tourist);
        } else {
            return false;
        }
    }
}
