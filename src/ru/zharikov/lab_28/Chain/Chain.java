package ru.zharikov.lab_28.Chain;

public abstract class Chain {
    private Chain next;

    public Chain linkWith(Chain next) {
        this.next = next;
        return next;
    }

    public abstract boolean check(Tourist tourist);

    protected boolean checkNext(Tourist tourist) {
        if (next == null) {
            return true;
        }
        return next.check(tourist);
    }
}

