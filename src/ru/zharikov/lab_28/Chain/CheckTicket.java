package ru.zharikov.lab_28.Chain;

public class CheckTicket extends Chain {

    public boolean check(Tourist tourist) {
        if (tourist.getHasTicket()) {
            return checkNext(tourist);
        } else {
            return false;
        }
    }
}
