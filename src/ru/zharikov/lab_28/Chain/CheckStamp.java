package ru.zharikov.lab_28.Chain;

public class CheckStamp extends Chain {

    public boolean check(Tourist tourist) {
        if (tourist.getHasStamp()) {
            return checkNext(tourist);
        } else {
            return false;
        }
    }
}
