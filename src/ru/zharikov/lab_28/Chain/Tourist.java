package ru.zharikov.lab_28.Chain;

public class Tourist {
    private Boolean hasPassport;
    private Boolean hasTicket;
    private Boolean hasVisa;
    private Boolean hasStamp;
    private Boolean dateIsValid;

    public Tourist() {
    }

    public Tourist(Boolean hasPassport, Boolean hasTicket, Boolean hasVisa, Boolean hasStamp, Boolean dateIsValid) {
        this.hasPassport = hasPassport;
        this.hasTicket = hasTicket;
        this.hasVisa = hasVisa;
        this.hasStamp = hasStamp;
        this.dateIsValid = dateIsValid;
    }

    public Boolean getHasPassport() {
        return hasPassport;
    }

    public void setHasPassport(Boolean hasPassport) {
        this.hasPassport = hasPassport;
    }

    public Boolean getHasTicket() {
        return hasTicket;
    }

    public void setHasTicket(Boolean hasTicket) {
        this.hasTicket = hasTicket;
    }

    public Boolean getHasVisa() {
        return hasVisa;
    }

    public void setHasVisa(Boolean hasVisa) {
        this.hasVisa = hasVisa;
    }

    public Boolean getHasStamp() {
        return hasStamp;
    }

    public void setHasStamp(Boolean hasStamp) {
        this.hasStamp = hasStamp;
    }

    public Boolean getDateIsValid() {
        return dateIsValid;
    }

    public void setDateIsValid(Boolean dateIsValid) {
        this.dateIsValid = dateIsValid;
    }
}
