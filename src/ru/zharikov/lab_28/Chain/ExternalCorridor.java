package ru.zharikov.lab_28.Chain;

public class ExternalCorridor extends InternalCorridor {
    @Override
    public boolean check(Tourist tourist) {
        if (super.check(tourist)) {
            CheckVisa checkVisa = new CheckVisa();
            checkVisa.linkWith(new CheckStamp().linkWith(new CheckDate()));
            return checkVisa.check(tourist);
        } else {
            return false;
        }

    }
}
