package ru.zharikov.lab_28.Chain;

public class CheckDate extends Chain {

    public boolean check(Tourist tourist) {
        if (tourist.getDateIsValid()) {
            return checkNext(tourist);
        } else {
            return false;
        }
    }
}
