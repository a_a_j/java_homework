package ru.zharikov.lab_28.Chain;

public class Main {
    public static void main(String[] args) {
        Tourist tourist = new Tourist(true, true, true, false, false);
        InternalCorridor internalCorridor = new InternalCorridor();
        System.out.println(internalCorridor.check(tourist));
        ExternalCorridor externalCorridor = new ExternalCorridor();
        System.out.println(externalCorridor.check(tourist));
    }
}
