package ru.zharikov.lab_28.Adapter;

import java.util.Arrays;

public class DialogWindow {
    public final int WIDTH = 500;
    public final int HEIGHT = 300;

    public DialogWindow() {
    }

    public String open() {
        return "Opening dialog window";
    }

    public String close() {
        return "Closing dialog window";
    }

    /*@Override
    public String getParams() {
        return "DialogWindow{" +
                "width=" + WIDTH +
                ", height=" + WIDTH +
                ", caption='" + open() + '\'' +
                '}';
    }*/
}
