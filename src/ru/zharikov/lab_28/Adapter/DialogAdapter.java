package ru.zharikov.lab_28.Adapter;

public class DialogAdapter extends StandardWindow {
    DialogWindow dialogWindow;

    public DialogAdapter(DialogWindow dialogWindow) {
        this.dialogWindow = dialogWindow;
    }

    @Override
    public String getParams() {
        return "DialogWindow{" +
                "width=" + dialogWindow.WIDTH +
                ", height=" + dialogWindow.WIDTH +
                ", caption='" + dialogWindow.open() + '\'' +
                '}';
    }
}
