package ru.zharikov.lab_28.Adapter;

public class Main {
    public static void main(String[] args) {
        StandardWindow standardWindow = new StandardWindow(500, 300, "White", "Standart window", null);
        Drawer drawer = new Drawer();
        drawer.toDraw(standardWindow);
        DialogWindow dialogWindow = new DialogWindow();
        drawer.toDraw(new DialogAdapter(dialogWindow));
    }
}
