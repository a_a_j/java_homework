package ru.zharikov.lab_28.Adapter;

import java.util.Arrays;

public class StandardWindow {
    private int width;
    private int height;
    private String color;
    private String caption;
    private Button[] buttonsArray;

    public StandardWindow() {

    }

    public StandardWindow(int width, int height, String color, String caption, Button[] buttonsArray) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.caption = caption;
        this.buttonsArray = buttonsArray;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Button[] getButtonsArray() {
        return buttonsArray;
    }

    public void setButtonsArray(Button[] buttonsArray) {
        this.buttonsArray = buttonsArray;
    }

    public String getParams() {
        return "StandardWindow{" +
                "width=" + width +
                ", height=" + height +
                ", color='" + color + '\'' +
                ", caption='" + caption + '\'' +
                ", buttonsArray=" + Arrays.toString(buttonsArray) +
                '}';
    }
}
