package ru.zharikov.homework1.task3;

import java.util.Scanner;

// Написать программу для вывода на экран таблицы умножения до введенного пользователем порога.

public class Main {
    public static void main(String[] args) {
        int x, y;
        StringBuilder str = new StringBuilder();
        System.out.println("Пороговое число X:");
        x = new Scanner(System.in).nextInt()+1;
        System.out.println("Пороговое число Y:");
        y = new Scanner(System.in).nextInt()+1;
        int[][] arrayTable = new int[y][x];

        for (int i = 1; i < y; i = i + 1) {
            str.setLength(0);
            for (int j = 1; j < x; j = j + 1) {
                arrayTable[i][j] = i*j;
                str.append("   ").append(arrayTable[i][j]);
            }
            System.out.println(str);
            System.out.println();
        }
    }
}
