package ru.zharikov.homework1.task2;

import java.util.Arrays;
import java.util.Scanner;

// Написать программу для поиска минимального из 4 чисел.

public class Main {
    public static void main(String[] args) {
        int[] someNumbers = new int[4];
        int i = 0;
        Scanner console = new Scanner(System.in);
        while (i < 4) {
            System.out.println("Введите число №" + (i+1) + ":");
            if (console.hasNextInt()) {
                someNumbers[i] = console.nextInt();
                i = i + 1;
            }
        }
        System.out.println("Минимальное число: " + Arrays.stream(someNumbers).min().getAsInt());
    }
}
