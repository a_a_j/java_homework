package ru.zharikov.homework1.task1;

import java.util.Scanner;

/*
Написать программу, которая считает стоимость бензина
(на вход программе передается кол-во литров, на выходе печатается стоимость).
Пример: стоимость литра бензина 43 рубля. На вход подается 50, на выходе должно быть 2150 руб.
*/

public class Main {
    public static void main(String[] args) {
        int literPrice = 43;
        System.out.println("Введите кол-во литров:");
        int literQty = new Scanner(System.in).nextInt();
        System.out.println("Стоимость: " + (literPrice * literQty) + " р.");
    }
}
