package ru.zharikov.homework1.task4;

// Написать программу, которая выводит факториал числа N.

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int num, res=1;
        StringBuilder str = new StringBuilder();
        System.out.println("Введите число:");
        num = new Scanner(System.in).nextInt();
        for (int j = 1; j <= num; j = j + 1) {
            res = res*j;
            str.append(res).append(", ");
        }
        System.out.println("Факториал: " + str.substring(0, str.length()-2));
    }
}
