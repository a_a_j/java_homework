The implication that Apple is exhibiting some monopolistic urge to gutshot Facebook and Google
makes close to zero sense.
The events of this week will not affect their bottom lines,
and Apple could have taken much more drastic action to lock down iOS — as it has before.
That it didn’t tells us everything about one of the most important systems of checks and balances
in consumer technology today.
Apple’s power only goes so far. It needs to offer Facebook and Google apps, because billions of people want them.
Consider that Apple didn’t remove any Facebook or Google apps from its App Store. Revoking the enterprise certificates
may have disabled the iOS research apps and internal company functions, like the Gbus program that helps Google
employees shuttle around their campus, but that’s no big deal. The data harvesting continued in the usual places — 
your Facebook News Feed and Google search, for example.