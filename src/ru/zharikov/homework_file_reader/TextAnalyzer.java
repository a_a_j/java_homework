package ru.zharikov.homework_file_reader;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TextAnalyzer {
    java.io.File file;

    // конструктор
    public TextAnalyzer(String filePath) {
        file = new java.io.File(filePath);
    }

    // сохранение уникальных слов в текстовый файл
    public void saveUniqueWords(String filePath) {
        Set<String> uniqueWords = new HashSet<>();
        try {
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\\s");
            while(sc.hasNext()){
                uniqueWords.add(sc.next().toLowerCase());
            }

            FileWriter writer = new FileWriter(filePath);
            for(String str: uniqueWords) {
                writer.write(str+"\r\n");
            }

            writer.close();
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
