package ru.zharikov.homework_file_reader;

/* Задание 1 (пакет 1)
Написать программу, читающую текстовый файл.
Программа должна составлять отсортированный по алфавиту список слов,
найденных в файле и сохранять его в файл-результат. Найденные слова не должны повторяться.
Оптимально приводить их к одному регистру. Одно слово в разных падежах это разные слова.*/

public class Main {
    public static void main(String[] args) {
        // Внимание: пути прописаны на MacOS, на Windows пути могут отличаться!
        TextAnalyzer textAnalyzer = new TextAnalyzer("src/ru/zharikov/homework_file_reader/text.txt");

        // файл unique_words.txt создается внутри пакета
        textAnalyzer.saveUniqueWords("src/ru/zharikov/homework_file_reader/unique_words.txt");
    }
}
