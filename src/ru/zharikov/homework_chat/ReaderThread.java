package ru.zharikov.homework_chat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ReaderThread extends Thread {
    int port;

    public ReaderThread(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        String inputMsg;
        try (ServerSocket sSocket = new ServerSocket(port)) {
            System.out.println("Reader is running.");
            try (Socket socket = sSocket.accept();
                 BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
                while (true) {
                    inputMsg = in.readLine();
                    if (!inputMsg.isEmpty()) {
                        System.out.println(inputMsg);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
