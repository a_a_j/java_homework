package ru.zharikov.homework_chat;

// запуск тредов на чтение и передачу
public class Chat {

    public Chat(int readerPort, int writerPort) {
        ReaderThread reader = new ReaderThread(readerPort);
        reader.start();

        WriterThread writer = new WriterThread(writerPort);
        writer.start();
    }

}
