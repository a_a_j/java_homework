package ru.zharikov.homework_chat;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class WriterThread extends Thread {
    int port;

    public WriterThread(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        InetAddress addr = null;
        try {
            addr = InetAddress.getByName("127.0.0.1");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        Socket socket = null;
        // ждем сокет собеседника
        while (socket == null) {
            try {
                socket = new Socket(addr, port);
            } catch (IOException e) {}
        }

        System.out.println("Writer is running...");

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            String outputStr = "";

            while (true) {
                outputStr = new Scanner(System.in).next();
                out.println(outputStr);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
