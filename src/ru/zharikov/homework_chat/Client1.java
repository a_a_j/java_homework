package ru.zharikov.homework_chat;

/* Задание 3 (пакет 3)
Создать программу, при запуске двух экземпляров которой (возможно, с разными параметрами)
эти экземпляры будут обмениваться сообщениями.
Обеспечить асинхронную передачу сообщений между клиентом и сервером
(любой из них может в любой момент отправить сообщение другому). */

public class Client1 {
    public static void main(String[] args) {
        new Chat(8764, 8765);
    }
}
