package ru.zharikov.lab_write_read_obj;

public class Obj {
    public int paramInt;
    public String paramStr;
    public double paramDbl;

    public Obj(int paramInt, String paramStr, double paramDbl) {
        this.paramInt = paramInt;
        this.paramStr = paramStr;
        this.paramDbl = paramDbl;
    }

    public Obj() {
    }

    public int getParamInt() {
        return paramInt;
    }

    public void setParamInt(int paramInt) {
        this.paramInt = paramInt;
    }

    public String getParamStr() {
        return paramStr;
    }

    public void setParamStr(String paramStr) {
        this.paramStr = paramStr;
    }

    public double getParamDbl() {
        return paramDbl;
    }

    public void setParamDbl(double paramDbl) {
        this.paramDbl = paramDbl;
    }
}
