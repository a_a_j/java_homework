package ru.zharikov.lab_write_read_obj;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ParseObj {
    Object obj;
    List<String> objParams = new ArrayList<>();

    public ParseObj(Object obj) {
        this.obj = obj;
    }

    public void SaveObjToFile(String filePath){
        Class c =  obj.getClass();
        for (Field f:c.getDeclaredFields()) {
            objParams.add(f.toString());
        }

        OutputStream os = null;
        try {
            os = new FileOutputStream(new File(filePath));
            os.write(objParams.toString().getBytes(), 0, objParams.toString().length());
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Obj LoadObjFromFile(String filePath) throws FileNotFoundException {
        List<Field> fields = new ArrayList<>();
        Obj newObj = new Obj();
        File file = new File(filePath);
        Scanner sc = new Scanner(file);
        while (sc.hasNext()) {

        }

        return newObj;
    }
}
