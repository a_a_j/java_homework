package ru.zharikov.lab_write_read_obj;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Obj myObj = new Obj(12, "ABC", 7.2);
        Obj newObj = new Obj();
        ParseObj parseObj = new ParseObj(myObj);
        parseObj.SaveObjToFile("/Volumes/Files/Documents/Java/java_homework/src/ru/zharikov/lab_write_read_obj/test.txt");
        newObj = parseObj.LoadObjFromFile("/Volumes/Files/Documents/Java/java_homework/src/ru/zharikov/lab_write_read_obj/test.txt");

    }
}
