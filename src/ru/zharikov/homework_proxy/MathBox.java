package ru.zharikov.homework_proxy;

import java.util.*;

//@ClearData
public class MathBox implements MathInterface {
    Set<Integer> intSet = new HashSet<>();

    public MathBox(int[] intArray) {
        for (int i:intArray) {
            intSet.add(i);
        }
    }

    @Override
    public void addItem(int i) {
        intSet.add(i);
    }

    @Override
    public void removeItem(int i) {
        intSet.remove(i);
    }

    @Override
    public int getTotal() {
        int sum = 0;
        for (int i : intSet) {
            sum = sum + i;
        }
        return sum;
    }

    @Override
    public double getAverage() {
        return ((double) getTotal() / intSet.size());
    }

    @Override
    public Set<Integer> getSet() {
        return intSet;
    }

    @Override
    public Set<Integer> getMultiplied(int multiplicator) {
        Set<Integer> multiSet = new HashSet<>();
        for (int i:intSet) {
            multiSet.add(i*multiplicator);
        }
        intSet = multiSet;
        return intSet;
    }

    @Override
    public Map<Integer, String> getMap() {
        Map<Integer,String> strMap = new HashMap<>();
        for (int i:intSet) {
            strMap.put(i, String.valueOf(i));
        }
        return strMap;
    }

    @Override
    public int getMax() {
        return Collections.max(intSet);
    }

    @Override
    public int getMin() {
        return Collections.min(intSet);
    }

    @Override
    public void clear() {
        intSet.clear();
    }

    @Override
    @ClearData
    public void testMethod1() {
        System.out.println("Вызов метода, после которого последует очистка коллекции");
    }

    @Override
    @LogData
    public void testMethod2() {
        System.out.println("Вызов метода, до и после которого выводится коллекция");
    }
}
