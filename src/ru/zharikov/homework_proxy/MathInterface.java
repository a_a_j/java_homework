package ru.zharikov.homework_proxy;

import java.util.Map;
import java.util.Set;

public interface MathInterface {
    public void addItem(int i);
    public void removeItem(int i);
    public int getTotal();
    public double getAverage();
    public Set<Integer> getSet();
    public Set<Integer> getMultiplied(int multiplicator);
    public Map<Integer,String> getMap();
    public int getMax();
    public int getMin();
    public void clear();
    public void testMethod1();
    public void testMethod2();
}
