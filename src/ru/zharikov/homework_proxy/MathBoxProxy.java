package ru.zharikov.homework_proxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MathBoxProxy implements InvocationHandler {
    private MathBox mathBoxProxy;

    public MathBoxProxy(MathBox mathBox) {
        this.mathBoxProxy = mathBox;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // перебираем методы
        for (Method m:mathBoxProxy.getClass().getMethods()) {
            // если метод объекта совпадает по имени с вызываемым, смотрим его аннотации
            if (m.getName().equals(method.getName())) {
                Annotation logData = m.getAnnotation(LogData.class);
                Annotation clearData = m.getAnnotation(ClearData.class);
                // если есть аннотация LogData, то выводим коллекцию до и после вызова
                if (logData != null) {
                    System.out.println(mathBoxProxy.getSet());
                    Object result = method.invoke(mathBoxProxy, args);
                    System.out.println(mathBoxProxy.getSet());
                    return result;
                }
                // если есть аннотация ClearData, то после вызова метода очищаем коллекцию
                if (clearData != null) {
                    Object result = method.invoke(mathBoxProxy, args);
                    mathBoxProxy.clear();
                    return result;
                }
            }
        }
        return method.invoke(mathBoxProxy, args);
    }

}
