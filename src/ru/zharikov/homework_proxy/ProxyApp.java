package ru.zharikov.homework_proxy;

import java.lang.reflect.Proxy;
import java.util.Map;

public class ProxyApp {
    public static void main(String[] args) {
        int[] intArray = {1,2,2,3,3,3,4,4,4,4};
        MathBox mathBox = new MathBox(intArray);
        mathBox.removeItem(4);
        mathBox.addItem(5);
        mathBox.addItem(6);
        System.out.println("Сумма значений коллекции = " + mathBox.getTotal());
        System.out.println("Среднее значение в коллекции = " + mathBox.getAverage());

        System.out.println("Вывод коллекции до умножения = " + mathBox.getSet());
        System.out.println("Вывод коллекции после умножения = " + mathBox.getMultiplied(2));

        Map<Integer,String> strMap = mathBox.getMap();
        System.out.println("Вывод Map = " + strMap);

        System.out.println("Максимальное значение = " + mathBox.getMax());
        System.out.println("Минимальное значение = " + mathBox.getMin());

        // создаем прокси-объект
        MathBoxProxy mathBoxProxy = new MathBoxProxy(mathBox);
        MathInterface stc = (MathInterface) Proxy.newProxyInstance(
                MathBoxProxy.class.getClassLoader(),
                new Class[]{MathInterface.class},
                mathBoxProxy);
        System.out.println();

        // вызываем метод, имеющий аннотацию LogData
        stc.testMethod2();
        System.out.println();

        // вызываем метод, имеющий аннотацию ClearData
        stc.testMethod1();
        System.out.println(stc.getSet());
    }

}
