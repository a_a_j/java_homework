package ru.zharikov.lab_deadlock;

public class Thread1 extends Thread{
    Thread thread2;

    public Thread1() {
    }

    public void setThread2(Thread thread2) {
        this.thread2 = thread2;
    }

    @Override
    public void run() {
        try {
            thread2.wait(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
