package ru.zharikov.lab_deadlock;

public class MainApp {
    public static void main(String[] args) {
        Thread thread1 = new Thread1();
        Thread thread2 = new Thread2();

        ((Thread1) thread1).setThread2(thread2);
        ((Thread2) thread2).setThread1(thread1);

        thread1.start();
        thread2.start();
    }

}
