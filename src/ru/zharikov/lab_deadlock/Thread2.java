package ru.zharikov.lab_deadlock;

public class Thread2 extends Thread{
    Thread thread1;

    public Thread2() {
    }

    public void setThread1(Thread thread1) {
        this.thread1 = thread1;
    }

    @Override
    public void run() {
        try {
            thread1.wait(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
