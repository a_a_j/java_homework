package ru.zharikov.homework_unique_chars;

import java.util.Map;
import java.util.HashMap;

public class UniqueChars {

    private String text;
    Map charMap;

    public String getText() {
        return charMap.toString();
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {
        charMap = new HashMap<Character, Integer>();
        for (int i = 0; i < text.length(); i++) {
            Integer keyValue = 1;
            char currentChar = text.charAt(i);
            if (charMap.containsKey(currentChar)) {
                keyValue = (int)charMap.get(currentChar);
                keyValue++;
            }
            charMap.put(currentChar, keyValue);
        }
    }
}